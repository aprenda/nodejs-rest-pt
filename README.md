## Curso de NodeJs

https://www.udemy.com/nodejs-rest-pt

## Site do Npm

https://www.npmjs.com/

## Iniciando a criação do pacote npm

`$ npm init -y`

## Instalando o Yargs

`$ npm install yargs --save`

## Instalando o Typescript

`$ npm install typescript -g`

## Criando o arquivo de configuração do Typescript

`$ tsc --init`

## Instalando as definições de tipo do node para typescript

`$ npm install @types/node --save-dev`

## Instalando as definições de tipo do yargs para typescript

`$ npm install @types/yargs --save-dev`

## Compilando o arquivo Typescript

`$ tsc`

## Compilando o arquivo Typescript sempre que houver alteração

`$ tsc -w`

## Debugando o type script

Adicionando esta configuração no tsconfig.json

"sourceMap": true,

`$ node --inspect-brk dist/main.js --num=5`

Acessando o Google Chrome
chrome://inspect

## Onde parei:

https://www.udemy.com/nodejs-rest-pt/learn/v4/t/lecture/9649358?start=0