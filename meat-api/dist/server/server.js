"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restify = require("restify");
const environment_1 = require("../common/environment");
const mongoose = require("mongoose");
class Server {
    initializeDb() {
        mongoose.Promise = global.Promise;
        return mongoose.connect(environment_1.environment.db.url, {
            useMongoClient: true
        });
    }
    initRoutes(routers = []) {
        return new Promise((resolve, reject) => {
            try {
                /**
                 * Criando o server
                 */
                this.application = restify.createServer({
                    name: 'meat-api',
                    version: '1.0.0'
                });
                /**
                 * Configurando para o restify fazer o parser das querys de urls
                 * e entregar um objeto
                 */
                this.application.use(restify.plugins.queryParser());
                /**
                 * Configurando para o restify fazer o parser do body json
                 * e entregar um objeto
                 */
                this.application.use(restify.plugins.bodyParser());
                // Rotas
                for (let router of routers) {
                    router.applyRoutes(this.application);
                }
                /**
                 * Criando a primeira rota
                 */
                // this.application.get('/info', (req, resp, next) => {
                //     // resp.setHeader('Content-Type','application/json')
                //     // resp.status(400)
                //     // resp.send({message:'hello'})
                //     resp.json(
                //         {
                //             message: 'Informações',
                //             browser: req.userAgent(),
                //             method: req.method,
                //             url: req.href(),
                //             path: req.path(),
                //             query: req.query,
                //         }
                //     )
                //     return next()
                // })
                /**
                 * Ouvir em uma porta e registrar um listening
                 */
                this.application.listen(environment_1.environment.server.port, () => {
                    resolve(this.application);
                });
            }
            catch (error) {
                reject(error);
            }
        });
    }
    bootstrap(routers = []) {
        return this.initializeDb().then(() => this.initRoutes(routers).then(() => this));
    }
}
exports.Server = Server;
