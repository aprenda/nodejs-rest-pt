# Iniciando o projeto

`npm install -y`

Instalando a dependencia restify

`npm install restify@6.3.4 --save -E`

Definições de tipos para o restify

`npm install @types/restify@5.0.6 -D -E`

Instalando o nodemon para executar o script quando modificado

`npm install nodemon -g`

Start o nodemon

`nodemon dist/main.js`

# Instalando o MongoDB

https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-18-04

# Acessando o mongodb via terminal

`mogodb`

Mostrando o banco selecionado

`db`

Selecionando outro banco de dados

`use nome_do_banco`

Cadastrando um objeto

`var diana = {name: 'Diana', email: 'diana@dc.com'}`

`db.heroes.insert(diana)`

Consultando o resultado

`db.heroes.find()`

Consultando um único resultado

`db.heroes.findOne()`

Atualizando um registro

`var peter = db.heroes.findOne()`
`peter.name = 'Peter Benjamin Parker'`
`peter.age = 15`
`db.heroes.update({_id: peter._id}, peter)`

Atualizando parcialmente

`db.heroes.update({_id: peter._id},{"$set": {name: 'Peter'}})`

Removendo um registro

`db.heroes.remove({_id: peter._id})`

# Instalando o Robo 3T

https://snapcraft.io/robo3t-snap

# Instalando o Mongoose para se conectar com o mongo

`npm install mongoose@4.13.9 -P -E`

Instalando as definições de tipos

`npm install @types/mongoose@4.7.32 -D -E`

# Parei em:
https://www.udemy.com/nodejs-rest-pt/learn/v4/t/lecture/9649486?start=0